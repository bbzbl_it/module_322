# Auftrag 2 

## Drei Views skizzieren und beschreiben

Dieser Teil des Auftrags wird in der Praxis oft durch UI-Designer bearbeitet.

Sie erstellen drei Skizzen von Nicht-Trivialen-Views gemäss Themenreservation, d.h. Views, die minimal rund ein Dutzend Elemente aufweisen.  Diese drei Views müssen eine Zusammenhang haben und eine Folge von Bearbeitungsschritten aufweisen. Es ist darauf zu achten, dass die Views an sich unterschiedlich gestaltet werden können. Es sollen also nicht drei sehr ähnliche Views mit lediglich anderen Daten sein. Die Skizzen sind entweder auf Papier (weniger genau, Abbildung via Smartphone) oder am Computer (genau, Abbildung der Screenshots) anzufertigen.

Sie beschreiben die Views, deren Funktionen und wie zwischen Ihnen navigiert werden kann, so dass andere Personen dies nachvollziehen können.

  

Mögliche Tools zur Erstellung der Skizzen
- Adobe XD CC
- Pencil Project ([https://pencil.evolus.vn/](https://pencil.evolus.vn/))
- [https://www.figma.com/](https://www.figma.com/)
- [https://excalidraw.com/](https://excalidraw.com/)
- [https://mockflow.com/](https://mockflow.com/)
- [https://wireframe.cc/](https://wireframe.cc/)
- [https://online.visual-paradigm.com](https://online.visual-paradigm.com/)
- [https://moqups.com/](https://moqups.com/)
- [https://balsamiq.com/](https://balsamiq.com/)

![[A2.png]]
