# Auftrag 8 
## Dokumentation der Views und Verbesserungen aus Aufträgen 4, 5 und 6

Dieser Teil des Auftrags wird in der Praxis teils von Business Analysten und Softwareentwicklern ausgeführt.

Sie definieren mindestens zwei Verbesserungen zur Umsetzung, welche Sie aufgrund der Aufträge 4, 5 und 6 identifizieren konnten. 

Sie bilden sowohl Ihre ursprünglichen Skizzen der Views als auch die implementierten Views ab und stellen diese in der Dokumentation gegenüber (Soll/Ist).

Unterhalb der nebeneinander dargestellten Skizzen und implementierten Views kommentieren Sie Übereinstimmungen, Abweichungen und realisierte Verbesserungen.


![[A10.png]]

