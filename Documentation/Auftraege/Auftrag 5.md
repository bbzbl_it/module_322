# Auftrag 5 

## Views mit ISO 9241-110 prüfen

Dieser Teil des Auftrags wird in der Praxis oft durch UI- oder UX-Designer bearbeitet.

Sie analysieren Ihre Views mittels der ISO Norm 9241 110 und der dafür ausgewählten sieben Normaspekte. Sie können dies tabellarisch darstellen. Achten Sie darauf, dass Sie die Normaspekte konkret auf View-Details beziehen. Beschreiben Sie, warum bestimmte Normaspekte erfüllt oder zu verbessern sind.

Wichtig: Verbesserungen müssen aktuell nur beschrieben werden. Die Skizzen müssen Sie nicht überarbeiten.

