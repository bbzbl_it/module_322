# Auftrag 6 
## Views mit einem Paperprototype und Testkandidat/in prüfen

Dieser Teil des Auftrags kann in der Praxis entweder durch Mitarbeitende eines Usability-Labs, durch UI- oder UX-Designer oder Entwickler bearbeitet werden.

Führen Sie einen Paperprototype-Test durch. Dies ist in zwei Rollen mit unterschiedlichen Aufgaben gefordert, die beide entsprechend zu dokumentieren sind:

1. Testverantwortliche Person: Sie bereiten Ihre Skizzen so vor, dass Sie diese einer/m Testkandidaten/in (der Name ist zu nennen) vorlegen können. Dabei stellen Sie diesen die ebenfalls vorher vorbereitete generelle Aufgabe und geben allenfalls Hinweise zu Teilschritten. In Ihrer Dokumentation beschreiben Sie Ihre Vorbereitungen, die Durchführung mit Reaktion der/des Testkandidaten/in zu den einzelnen Views und die Ergebnisse. Das Ziel ist, dass Sie mit den Tests aufzeigen, was verständlich war oder wo Verbesserungen nötig sind.  Ihren Prototypen bilden Sie mit Handy-Fotos ab.
    
2. Testkandidat/in: Sie beschreiben, wie es Ihnen als Testkandidat/in ergangen ist. Wie war es, die Aufgabe mit den Views zu lösen? Weitere Eindrücke? Gab es eine Erkenntnis, die auch für Ihr Projekt relevant ist? Nennen Sie den Namen der testverantwortlichen Person, die den Test mit Ihnen gemacht hat.
    

  
Wichtig: Verbesserungen müssen aktuell nur beschrieben werden. Die Skizzen müssen Sie nicht überarbeiten.


![[A6.png]]

