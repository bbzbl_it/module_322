# Auftrag 4 

## Views mit Gestalt-Gesetzen prüfen

Dieser Teil des Auftrags wird in der Praxis oft durch UI- oder UX-Designer bearbeitet.

Sie prüfen die Views mit den zwei Gestalt-Gesetzen der Nähe und der Ähnlichkeit. Sie zeigen entweder, warum und wie Ihre Views alle Regeln bereits mustergültig einhalten, oder welche Verbesserungen Sie ableiten. Erläutern Sie, warum die Gestalt-Gesetze gut angewandt sind oder wie diese verbessert werden können. Wichtig dabei ist, dass Sie sich konkret auf Ihre Views beziehen.

Wichtig: Verbesserungen müssen aktuell nur beschrieben werden. Die Skizzen müssen Sie nicht überarbeiten.
