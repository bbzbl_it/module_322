# Auftrag 3

## Ein erstes Programm kann in der gewählten Technologie implementiert und ausgeführt werden

Dieser Teil des Auftrags wird in der Praxis meist von Softwaredevelopers ausgeführt.

Demonstration: Am Termin gemäss Quartalsplan zeigen Sie der Lehrperson, dass Sie ein kleines "HelloWorld"-Programm in der von Ihnen gewählten Technologie ausführen können. Es kann sich auch bereits um die Basis für die effektive Realisierung der Views handeln.

__Anforderungen an Ihr Programm:__

Grundlagen: Sie können eine View öffnen

Fortgeschritten: Sie können mindestens drei Komponenten auf der View platzieren und anzeigen

Erweitert: Sie können zu einer zweiten View wechseln

Beachten Sie, dass dieser Auftrag mittels Demonstration beurteilt wird.