# Auftrag 7 
## Views implementieren

Dieser Teil des Auftrags wird in der Praxis meist von Softwareentwicklern ausgeführt.

Sie implementieren die skizzierten Views in der von Ihnen gewählten Technologie, wobei Sie zwei Verbesserungen gemäss Auftrag 8 berücksichtigen. Die Views sollen dabei möglichst genau den entworfenen Skizzen entsprechen. Die Navigation zwischen den Views muss implementiert werden.  
Sie geben das Projekt Ihrer Entwicklungsumgebung als ZIP-Datei ab.

Fachgespräch(e): Die Lehrperson kann während der Realisierung jederzeit in Ihren Code sehen und ein Fachgespräch mit Ihnen führen.

Demonstration: In der Woche gemäss Quaralsplan zeigen Sie der Lehrperson  Ihr Programm, dass Sie anhand der Skizzen der Views realisiert haben. Anhand der Dokumentation zeigen Sie die Abweichungen zwischen Skizzen und implementierten Views auf.