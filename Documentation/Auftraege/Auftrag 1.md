# Auftrag 1 

## Personas entwerfen

Der erste Teil dieses Auftrags wird in der Praxis oft durch die Marketingabteilung bearbeitet.

Recherchieren Sie, wie Sie etwas über die Nutzer Ihrer Applikation erfahren könnten. Dabei beachten Sie Themen wie Anforderungen, Kenntnisse und Erwartungen. Sie denken sich anhand der Punkte der Vorlage auf der Themenseite "Methoden und Personas" drei Personas aus. Diese beschreiben Sie im Dokument und dokumentieren Ihre Überlegungen.

Austausch + Reflexion (QW2): 

1. Wählen Sie  eine/n Partner/in und stellen sich zuerst Ihre Personas gegenseitig vor. Zeigen Sie auf, was die Auswirkungen Ihrer Persona auf Ihre Views sind, warum es wichtig ist, die Bedürfnisse der Benutzergruppe zu verstehen. 

2. Diskutieren Sie wie die Personas die Ausarbeitung der Views beeinflussen könnte, welche Herausforderungen auftreten könnten, und wie diese gelöst werden könnten.



## Lösungen

Hier kann man meine Personas, die ich erstellt habe sehen:

- [[Persona1.pdf]]
- [[Persona2.pdf]]
- [[Persona3.pdf]]
