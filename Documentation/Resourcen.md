# Resourcen

Hier sind die einzelnen Resourcen die ich für das Modul gesamelt habe.


## List

| Name | Link | Beschreibung |
|---|---|---|
| Methoden | [[1_Methoden]] | Methoden auf dem Weg von einer Idee zu einem Programm |
| Personas | [[2_Personas]] | Personas erstellen, um zu verstehen und zu beschreiben, wer eine zukünftige Software bedienen wird |


