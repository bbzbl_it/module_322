# Aufträge

Diese Seite stellt die einzelnen Aufträge zum Beurteilungsraster vor. 

Begriff Views: Views wird in diesem Modul für Programmfenster, Webseiten, etc. verwendet.

Organisation: Es werden jeweils ein oder mehrere Aufträge bis zu einem Termin gemäss Quartalsplanung erarbeitet und abgegeben. Die Abgabe der Aufträge erfolgt via Google Drive Ordner (siehe Link via Klassen), die Anforderungen stehen unter den Aufträgen.

Beachten Sie, dass die Aufträge 3 und 7 ganz resp. teilweise als Demonstration für die Lehrperson nachzuweisen sind.

Werden Aufträge zu spät abgegeben, so erhalten Sie einen angemessenen Notenabzug (in Abhängigkeit der Verspätung). Sind Sie an einem Abgabe- oder Demonstrationstermin abwesend, so holen Sie dies am nächsten oder einem separaten Termin nach Entscheid der Lehrperson nach. Solange nicht alle Aufträge erbracht sind, ist das Modul nicht abgeschlossen.

Dokumente: Die geforderten Dokumente sind jeweils einfach ein PDF-Dokument, welches mindestens die Modulnummer, -Titel, Klasse, Vorname und Name, Datum und Seitenzahlen aufweist und die Rechtschreibung berücksichtigt.

### Themenreservation

In diesem Modul entwerfen Sie mindestens drei Views, beurteilen und verbessern Ihren Entwurf aufgrund verschiedener Themen des Moduls und setzen die geplante Applikation danach in einer der vorgegebenen Technologien um.  Dabei geht es nicht darum, dass die Applikation vollständig funktionsfähig ist. Die grafische Oberfläche steht im Vordergrund sowie die Navigation zwischen den Views.

Dafür brauchen Sie als erstes eine Projektidee: Sie formulieren eine Aufgabenstellung, welche einen Nutzungskontext (Benutzer, Arbeitsaufgaben, Arbeitsmittel) beschreibt, drei Arbeitsschritte umfasst und reservieren dies als Thema bei Ihrer Lehrperson.


[[M322_W01_0_Praesentation-F9-16.pdf]]



### Abgabe Aufträge 1 + 2 

Sie geben Ihre Lösungen für die Aufträge 1 und 2 als ein Dokument ab.

Abgabe Auftrag 1: Personas inkl. Beschreibung, Kurzbericht zu Austausch und Reflexion

Abgabe Auftrag 2: Aufgabenstellung mit Nutzungskontext, Beschreibung von drei zusammenhängenden Arbeitsschritten, Abbildung der drei Skizzen mit Beschreibung der Funktion.


### Abgabe Aufträge 4, 5, 6

Sie geben Ihre Lösungen für die Aufträge 4, 5 und 6 als ein Dokument ab.

Abgabe Auftrag 4: Erläuterung der Bewertung der drei Views anhand der Gestalt-Gesetze Nähe und Ähnlichkeit inkl. möglichen Verbesserungen.

Abgabe Auftrag 5: Erläuterung für Ihre Views, wie gut oder schlecht diese die sieben Punkte der Norm ISO 9241-110:2006 erfüllen inkl. möglichen Verbesserungen.

Abgabe Auftrag 6: Sie erläutern die Aufgaben, welche Ihr/e Testkandidat/in lösen musste, wie gut oder schlecht er/sie mit den Views zurecht kam und wo Verbesserungen nötig sind. Sie stellen sich als Testkandidat/in zur Verfügung und beschreiben, wie dies für Sie war.

  
Details siehe Beschreibung in den einzelnen Aufträgen.


### Abgabe Aufträge 7 + 8

Abgabe Auftrag 7 : Sie geben das Projekt als eine ZIP-Datei des entsprechenden Projekt-Verzeichnisses ab.

Beachten Sie, dass Auftrag 7 auch eine Demonstration enthält.

Abgabe Auftrag 8: Sie erläutern in einem Dokument zwei mögliche Verbesserungen. Sie stellen darin weiter skizzierte Views und implementierte Views einander gegenüber, so dass diese auf einer Seite direkt verglichen werden können. Sie kommentieren Ihre Übereinstimmungen, Abweichungen und Verbesserungen.

Sie geben das ZIP-File und das Dokument zu Auftrag 8 separat als zwei Dateien ab.



### Abgabe Aufträge 7 + 8

Abgabe Auftrag 7 : Sie geben das Projekt als eine ZIP-Datei des entsprechenden Projekt-Verzeichnisses ab.

Beachten Sie, dass Auftrag 7 auch eine Demonstration enthält.

Abgabe Auftrag 8: Sie erläutern in einem Dokument zwei mögliche Verbesserungen. Sie stellen darin weiter skizzierte Views und implementierte Views einander gegenüber, so dass diese auf einer Seite direkt verglichen werden können. Sie kommentieren Ihre Übereinstimmungen, Abweichungen und Verbesserungen.

Sie geben das ZIP-File und das Dokument zu Auftrag 8 separat als zwei Dateien ab.