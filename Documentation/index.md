
# Module 322 Documentation

Dies ist meine Documentation für das Modul 322. 


## Inhalte

| Name | Link | Beschreibung | 
|---|---|---|
| Aufträge | [[Auftraege]] | Eine Seite für die Sammlung der einzelnen Aufträge. |
| Resourcen | [[Resourcen]] | Eine Seite für die Sammlung der einzelnen Resourcen. |
| Projekt | [[Projekt]] | Die Beschreibung und Definition des Projekts. |


## Changelog

| Datum | Tätigkeit |
|---|---|
| 01/11/2023 | Erstellen der mkdocs Webseite und iniziieren des Repos |
| 04/11/2023 | add Personas and Views |

