# Personas

Es führt Sie anschliessend in die Arbeit mit Personas ein, um besser zu verstehen und zu beschreiben, wer eine zukünftige Software bedienen wird.


## Ziele

- Sie kennen das Konzept der Personas als Möglichkeit Zielgruppen zu beschreiben.


## Personas = Beschreibung der Zielgruppen

Fast alle Produkte haben bestimmte Zielgruppen. Wenn man sich mit deren Zielen und Erwartungen beschäftigen will, wird häufig das Verfahren der Beschreibung von Personas angewandt. Man versucht mit möglichst wenigen, oft drei bis fünf Personas, sich die verschiedenen Zielgruppen besser vorstellen zu können.

### Personas können helfen folgende Fragen zu beantworten:

- Endkunden: Wer ist an unseren Produkten, Dienstleistungen interessiert? Welche Erwartungen bestehen?
- Handel: Wer ist (allenfalls häufig) unser Verhandlungspartner? Welche Erwartungen bestehen?

### Allgemeiner Ablauf um Personas zu definieren, die je nach Produkt- und Kundenart anzupassen sind:

1. Festlegen, welche Angaben bezüglich einer Persona relevant sind. Nachfolgend finden Sie einen anpassbaren Vorschlag.
2. Beschreiben Sie Hobbies und Lebensziele der Personengruppe.
3. Beschreiben Sie, was die Personas motiviert.
4. Was sind die Bedürfnisse der Personas.
5. Erwecken Sie Ihre Personas zum leben mit einer Skizze oder beispielhaftem Foto.


## Beispielhafte Kriterien für Personas

- Soziodemgraphische Merkmale
    - Geschlecht, Alter, Zivilstand, fiktiver Name, Lebensmittelpunkt (Stadt, Land,...), Werdegang, Beruf-Aufgaben-Position, Einkommen, Hobbys, Interessen, Lebensstil
- Erwartungen an Produkte, Informationswege
    - Beispiel: Erwartungen an Reaktionszeit, Lösungsqualität
- Charaktereigenschaften
    - Analog / Technologieenthusiast
- Aufgaben, Ziele, Wünsche
    - berufliche/ private Aufgaben, private/ berufliche Ziele, Wünsche, grundlegende Bedürfnisse

  
## Einige Regeln zur Erarbeitung von Personas

- Versuchen Sie typische, mögliche Personas für Ihre Anwendung zu definieren. In der Praxis werden diese hingegen aus Befragungsdaten abgeleitet und durch erneute Befragungen validiert.
- Fokussieren Sie auf die Aufgaben und den Nutzungskontext der Persona (Was will die Persona erreichen, in welcher Situation ist sie gerade, welche Bedürfnisse und Fähigkeiten stehen ihr zur Verfügung?).
- Adressaten der Personas sind Mitarbeitende im Marketing oder Design, die sich um Layoutfragen, Design des UI, etc. kümmern.
- In verschiedenen Situationen kann es wichtig sein, regulatorische Anforderungen zu berücksichtigen, z.B. bestehen für medizinische Produkte bestimmte Anforderungen.

![[P.jpg|Personas Bild]]


## Vorlage

Mit der Vorlage links können Sie eigene Personas darstellen. Doch es gibt im Internet viele weitere Anregungen.

[[Vorlage Persona.pptx|Vorlage Persona]]


## Umsetzung im Modul

Grob betrachtet, folgt das Modul im grundsätzlichen Ablauf den vorgestellten Methoden. Aufgrund der Zeitbegrenzung ist es jedoch nicht möglich, die iterativen Elemente durchzuspielen. In den Aufgaben erarbeiten Sie jedoch verschiedene Elemente, welche in den Methoden enthalten sind.



## Folien

[[M322_W01_0_Praesentation-Personas.pdf]]