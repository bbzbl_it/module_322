# Methoden

Diese Seite gibt einen kurzen Überblick über Methoden auf dem Weg von einer Idee zu einem Programm. 

## Ziele

- Sie kennen Methoden resp. Prozesse, Abläufe, welche beschreiben, welche Projekt- resp. Arbeitsschritte durchlaufen werden können, um von einer Idee zu einem fertigen Programm zu gelangen.

## Methoden und Prozesse auf dem Weg von der Idee zum Programm

Es gibt unterschiedliche Methoden und Prozesse, wie man von einer Idee zu einem fertigen Programm gelangen kann.  Sie werden sehen, dass die vorgestellten, aktuellen Methoden iterativ angelegt sind, d.h. scheinbar gleiche Arbeitsschritte werden wiederholt ausgeführt, mit dem Ziel, dadurch bessere Lösungen zu erreichen. 

Gegen diese Methoden wird häufig das Argument angeführt, dass dies Zeit und Geld kostet, doch was kostet Software, welche die gesetzten Ziele nicht erreicht?

Die Motivation für diese Methoden liegt eben gerade auch darin, dass die Kosten für die Erstellung einer Software hoch sind und deren Nutzbarkeit (die Software ist verständlich und es besteht kein Widerstand gegen die Nutzung) sowohl einen Einfluss auf die Produktivität des Unternehmens wie auf die Motivation der Mitarbeitenden haben kann.

## User-Centered Design

Beim User-centered Design wird die Realisierung einer Software in Iterationen umgesetzt. Die Kernidee ist, die zukünftigen Nutzenden in der Entwicklung zu integrieren. So soll besser berücksichtigt werden, wer diese sind, was deren Aufgaben sind, was sie mit der Software erreichen wollen und müssen. Jede Iteration beginnt damit, den Kontext, v.a. in Bezug auf die zukünftigen Nutzer in welchem die späteren Nutzer die Software nutzen, zu identifizieren. Dazu gehört eine Beschreibung der Nutzenden, deren Aufgabe und Ansprüche sowie der Nutzungskontext. In der zweiten Phase werden die Anforderungen der Benutzer beschrieben. Hier geht es um die feineren Ansprüche, welche Voraussetzungen bestehen, in welchen Schritten wird die Aufgabe gelöst, welche Unterstützung der Nutzenden z.B. durch eine Hilfe ist notwendig. Diese Anforderungen werden in der dritten Phase in einem Design dargestellt,. Das Design selbst wird häufig ebenfalls in einem iterativen Prozess entwickelt. In der vierten und letzten Phase wird die Software in einer Evaluation geprüft.

![[UCD.png|User-Centered Design Bild]]


## DIN EN ISO 9241-210 Prozess zur Gestaltung gebrauchstauglicher  Systeme

In der DIN EN ISO 9241-210 Norm finden Sie den iterativen Ansatz wie im User-centered Design ebenfalls in einer ähnlichen Form.

![[DIN.png|DIN EN ISO Bild]]


## Design Thinking

Während andere Methoden die Benutzenden der Software bewusst einbeziehen, geht das Design Thinking noch einen Schritt weiter und regt multidisziplinäre Teams an, in denen neben Usern, Designern, Entwicklern auch Psychologen, Betriebswirtschaftler usw. teilnehmen. Dieser Ansatz konzentriert sich auf die Bedürfnisse der Benutzer und fördert die Zusammenarbeit, das Experimentieren und das iterative Arbeiten. Im Wesentlichen geht es darum, Probleme aus der Sicht der Nutzer zu verstehen und dann innovative Lösungen zu entwickeln. 

Verstehen: Beschreibung des Problems, der Rahmenbedingungen und möglicher Lösungen.

Beobachtung: Im Gespräch mit dem Kunden herausfinden, wie er das Problem bisher löst.

Definition: Synthese aus 1. und 2. stellt die bisherigen Erkenntnisse zusammen.

Ideen: Das Team sammelt, bewertet und priorisiert Ideen zur Lösung des Problems der Zielgruppe und Personas.

Prototyping: Mit einem typischen Kunden im Kopf entsteht ein Prototyp.

Testing: Hier wird von den zukünftigen Benutzenden Feedback über den Prototypen eingeholt.


![[DT.png|Design Thinking Bild]]

