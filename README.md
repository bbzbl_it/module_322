# Module_322


## Getting started

You can either look at the code in the Repo here, or you can visit https://bbzbl_it.gitlab.io/module_322 for a mkdocs site.


## More Resources

| Resource | Link |
|----------|------|
| My Docs | https://bbzbl_it.gitlab.io/module_322 |
| Modul Website | https://sites.google.com/bbzbl-it.ch/modul-322-hs2023-q2/home |
